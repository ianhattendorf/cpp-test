#include "libapp/math.hpp"

unsigned int Factorial(unsigned int number) {
	if (number == 0 || number == 1) {
		return 1;
	}
	return Factorial(number - 1) * number;
}

# cpp-test

[![Build status](https://gitlab.com/ianhattendorf/cpp-test/badges/master/build.svg)](https://gitlab.com/ianhattendorf/cpp-test/commits/master)
[![Coverage report](https://gitlab.com/ianhattendorf/cpp-test/badges/master/coverage.svg)](https://ianhattendorf.gitlab.io/cpp-test)

## Description

Project to experiment with C++ development tools.
